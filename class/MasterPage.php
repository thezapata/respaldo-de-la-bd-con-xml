<?php

/*
 * Clase para formar la página a través de un 
 * template
 */

/**
 * Description of MasterPage
 *
 * @author Jaime
 */
class MasterPage 
{
    public function __construct()
    {
        
    }
    
    public function __destruct() 
    {
        
    }
    
    public function getHead()
    {
        include '../pages/templates/head.php';
    }
    
    public function getHeader()
    {
        include '../pages/templates/header.php';
    }
    
    public function getNav()
    {
        include '../pages/templates/nav.php';
    }
    
    public function getScripts()
    {
        include '../pages/templates/scripts.php';
    }
    
    
    
}
