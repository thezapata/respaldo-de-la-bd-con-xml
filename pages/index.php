<?php
    include '../class/MasterPage.php';
    $template = new MasterPage();
?>

<!DOCTYPE html>
<!--
Página de inicio
-->
<html lang="es">
    <head>
        <?php
            $template->getHead();
        ?>
    </head>
    <body>
        <main class="container">
            <header id="header">
                <?php
                    $template->getHeader();
                ?>
            </header>

            <nav id="mainMenu">
                <?php
                    $template->getNav();
                ?>
            </nav>
        	
            <article id="content">
                <h2>Inicio</h2>

                <p>
                    Bienvenido a la aplicación de usuarios
                </p>

                <div id="dialog" title="Mensaje del sistema"></div>

            </article>
        </main>
        
        <?php
            $template->getScripts();
        ?>
        
    </body>
</html>
