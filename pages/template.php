<?php
    include '../class/MasterPage.php';
    $template = new MasterPage();
?>

<!DOCTYPE html>
<!--
Página de inicio
-->
<html lang="es">
    <head>
        <?php
            $template->getHead();
        ?>
    </head>
    <body>
        <main class="container">
            <header id="header">
                <?php
                    $template->getHeader();
                ?>
            </header>

            <nav id="mainMenu">
                <?php
                    $template->getNav();
                ?>
            </nav>
        	
            <article id="content">
                <!--Contenido de la página -->
                <h2>Título contenido</h2>

                

                <div id="dialog" title="Mensaje del sistema"></div>

            </article>
        </main>
        
        <?php
            $template->getScripts();
        ?>
        
    </body>
</html>
