<?php
    include '../class/MasterPage.php';
    $template = new MasterPage();
?>

<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html lang="es">
    <head>
        <?php
            $template->getHead();
        ?>
    </head>
    <body>
        <main class="container">
            <header id="header">
                <?php
                    $template->getHeader();
                ?>
            </header>

            <nav id="mainMenu">
                <?php
                    $template->getNav();
                ?>
            </nav>
        	
            <article id="content">
                <h2>Usuarios</h2>
                <div id="form">
                    <form name="frmData" id="frmData">
                        <label for="txtUser">Usuario</label>
                        <input type="text" name="txtUser" id="txtUser" maxlength="100">
                        <br>
                             
                        <input type="hidden" name="txtId" id="txtId" value="">
                        
                        <label for="txtName">Nombre</label>
                        <input type="text" name="txtName" id="txtName" maxlength="100">
                        <br>
                        
                        <label for="txtCorreo">Correo</label>
                        <input type="text" name="txtEmail" id="txtEmail" maxlength="100">
                        <br>
                        
                        <label for="txtImg">Nombre de la imagen</label>
                        <input type="text" name="txtImg" id="txtImg">
                        <br>
                        
                        <input type="button" name="btnSave" id="btnSave" value="Guardar">
                        <input type="button" name="btnNuevi" id="btnNuevo" value="Insertar">
                        <input type="reset" value="Restablecer">
                    </form>
                </div>
                <br>
                <div id="resultRecords"></div>

                <div id="dialog" title="Mensaje del sistema"></div>

            </article>
        </main>
        
        <?php
            $template->getScripts();
        ?>

        <script type="text/javascript">
        	$(function(){
                listRecords();
                $('#btnSave').click(function(){
                    saveRecord();
                });

        	});
                
                $(function(){
                listRecords();    
                  $('#btnNuevo').click(function(){
                      insertRecord();
                  });
                });

        	function listRecords()
        	{
                $.ajax({
                    'url': '../queries/userList.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function deleteRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/deleteRecordAjax.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog();
                        listRecords();
                    }
                });
            }


            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/findRecord.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtEmail').val(data.email);
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }


            function saveRecord()
            {
                $.ajax({
                    'url': '../queries/save-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                            autoOpen: true,
                            modal: true,
                            buttons: {
                                'Cerrar': function(){
                                    $(this).dialog('close');
                                }
                            }
                        });
                        listRecords();
                    }
                });
            }
            
            function insertRecord()
            {
                $.ajax({
                    'url': '../queries/insert-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                            autoOpen: true,
                            modal: true,
                            buttons: {
                                'Cerrar': function(){
                                    $(this).dialog('close');
                                }
                            }
                        });
                        listRecords();
                    }
                });
                
            }

        </script>
    </body>
</html>
