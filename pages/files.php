<?php
    include '../class/MasterPage.php';
    $template = new MasterPage();
?>

<!DOCTYPE html>
<!--
Manejo de archivos con PHP
-->
<html>
    <head>
        <?php
            $template->getHead();
        ?>
    </head>
    <body>
        <main class="container">
            <header id="header">
                <?php
                    $template->getHeader();
                ?>
            </header>

            <nav id="mainMenu">
                <?php
                    $template->getNav();
                ?>
            </nav>

            <article id="content">
                <h1>Archivos en PHP</h1>
                
                <form name="frmFiles" id="frmFiles">
                    <label for="txtName">Nombre archivo</label>
                    <input type="text" name="txtName" id="txtName">.txt
                    <br>
                    <label for="txaText">Texto archivo</label>
                    <br>
                    <textarea name="txaText" id="txaText"></textarea>
                    
                    <br>
                    <label for="cboFiles">Archivos disponibles</label>
                    <select name="cboFiles" id="cboFiles"></select>
                    
                    <label for="txtCountFiles">Archivos disponibles</label>
                    <input type="text" name="txtCountFiles" id="txtCountFiles" readonly="">
                    
                    <br>
                    <input type="button" name="btnDeleteFile" id="btnDeleteFile" value="Eliminar archivo">
                    <input type="button" name="btnCreateFile" id="btnCreateFile" value="Crear archivo">
                    <input type="button" name="btnBD" id="btnBD" value="Crear respaldo de BD">
                    <input type="button" name="btnAddFile" id="btnAddFile" value="Adicionar texto al archivo">
                </form>

                <div id="dialog" title="Mensaje del sistema"></div>
            </article>
        </main>
        
        <?php
            $template->getScripts();
        ?>
        
        <script type="text/javascript">
            $(function(){
                listFiles();

                $('#btnCreateFile').click(function(){
                    $.ajax({
                        url: '../queries/create-file.php',
                        type: 'POST',
                        dataType: 'json',
                        data: $('#frmFiles').serialize(),
                        success: function(data){
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                            listFiles();
                        }
                    });
                });


                $('#btnAddFile').click(function(){
                    $.ajax({
                        url: '../queries/add-file.php',
                        type: 'POST',
                        dataType: 'json',
                        data: $('#frmFiles').serialize(),
                        success: function(data){
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                    });
                });


                $('#btnDeleteFile').click(function(){
                    $.ajax({
                        url: '../queries/delete-file.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {'fileName': $('#cboFiles').val()},
                        success: function(data){
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                            listFiles();
                        }
                    });
                });

           
            
            $('#btnBD').click(function() {
                $.ajax({
                    url: '../queries/create-filebd.php',
                    type: 'POST',
                    dataType: 'json',
                    data: $('#frmFiles').serialize(),
                    success: function(data) {
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'cerrar': function() {
                                            $(this).dialog('close');
                                    }
                            }
                      });
                                listFiles();
                    }
                });
        });


            function listFiles()
            {
                $.ajax({
                    url: '../queries/list-files.php',
                    type: 'POST',
                    dataType: 'json',
                    success: function(data){
                        $('#cboFiles').html(data.options);
                        $('#txtCountFiles').val(data.numberFiles);
                    }
                });
            }
    });
        </script>
    </body>
</html>
